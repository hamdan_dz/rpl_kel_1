Schedule Project :

27 Jan 2020
- Melakukan survey terhadap calon pengguna
- Melakukan identifikasi kebutuhan calon pengguna

29 Jan 2020
- Melakukan identifikasi terhadap hasil survey

15 Feb 2020
- Melakukan Design Mockup aplikasi

20 Feb 2020
- Merancang design front-end dari mockup aplikasi yg telah dibuat

1 Mar 2020
- Merancang back-end dari front-end aplikasi yg telah dibuat

30 Mar 2020
- Melakukan Testing aplikasi

5 Apr 2020
- Melakukan debugging awal

10 Apr 2020
- Melakukan debugging akhir

20 Apr 2020
- Final Launching